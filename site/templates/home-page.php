<?php

/**
 * Page template
 *
 */

include("./head.inc");

?>

<div class="call-to-action">
    <div class="container text-center">
        <a href="#" data-toggle="modal" data-target="#video">
            <img class="img-responsive ca-video" src="http://placehold.it/653x372" alt="placeholder">
        </a>
        <div class="modal fade" id="video" tabindex="-1" role="dialog" aria-labelledby="video" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Video</h4>
                    </div>
                    <div class="modal-body">
                        <img src="http://placehold.it/653x372" alt="placeholder">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <div class="social-media">
            <a href="#">
                <img src="<?= $config->urls->templates?>assets/imgs/social/facebook.png" alt="facebook">
            </a>
            <a href="#">
                <img src="<?= $config->urls->templates?>assets/imgs/social/twitter.png" alt="twitter">
            </a>
            <a href="#">
                <img src="<?= $config->urls->templates?>assets/imgs/social/instagram.png" alt="instagram">
            </a>
            <a href="#m">
                <img src="<?= $config->urls->templates?>assets/imgs/social/gmail.png" alt="email">
            </a>
            <a href="#">
                <img src="<?= $config->urls->templates?>assets/imgs/social/vimeo.png" alt="vimeo">
            </a>
        </div>
        <div class="arrow-wrapper">
            <div class="arrow-down"></div>
        </div>
    </div>
</div>

<div class="home-main">
    <div class="container">
        <?= $page->body; ?>
    </div>
</div>

<div class="container recent-videos">
    <h2>Recent Videos</h2>
    <?php $videoCount = 0; ?>
    <?php foreach ($pages->get("/videos")->videos as $video): ?>
        <div class="col-sm-6">
            <div class="video-wrapper">
                <?= $video->video_embed; ?>
            </div>
        </div>
        <?php
        $videoCount++;
        if ($videoCount >= 4) break;
        ?>
    <?php endforeach; ?>
</div>

<?php

include("./foot.inc");


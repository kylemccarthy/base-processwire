<?php

/**
 * Page template
 *
 */

include("./head.inc");

?>

    <div class="page-main">
        <div class="container">
            <?= $page->body; ?>
        </div>
    </div>
    <div class="main-container container">
        <h2>Documents</h2>
        <?php foreach ($pages->get("/documents")->documents as $document): ?>
            <h3><?=$document->document_title?></h3>
            <? if (!is_null($document->document_summary)): ?>
                <p><?=$document->document_summary?></p>
            <? endif; ?>
            <a href="<?=$document->document->url?>">View/download document</a>
        <?php endforeach; ?>
    </div>

<?php

include("./foot.inc");


<?php

/**
 * Page template
 *
 */

include("./head.inc");

?>

    <div class="page-main">
        <div class="container">
            <?= $page->body; ?>
        </div>
    </div>
    <div class="main-container container">
        <h2>Survey</h2>
        <div class="video-wrapper">
            <?= $page->survey; ?>
        </div>
    </div>

<?php

include("./foot.inc");


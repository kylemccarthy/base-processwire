<?php

/**
 * Page template
 *
 */

include("./head.inc");

?>

    <div class="page-main">
        <div class="container">
            <?= $page->body; ?>
        </div>
    </div>
    <div class="main-container container links">
        <h2>Links</h2>
        <ul>
        <?php foreach ($pages->get("/links")->links as $link): ?>
            <li><a href="<?=$link->link_url?>"><?=$link->link_title?></a></li>
        <?php endforeach; ?>
        </ul>
    </div>

<?php

include("./foot.inc");


<?php 

/**
 * Page template
 *
 */

include("./head.inc"); 

?>

<div class="main-base">
    <div class="container">
        <?= $page->body; ?>
    </div>
</div>
<?php

include("./foot.inc"); 


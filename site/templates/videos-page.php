<?php

/**
 * Page template
 *
 */

include("./head.inc");

?>

<div class="page-main">
    <div class="container">
        <?= $page->body; ?>
    </div>
</div>
<div class="main-container container recent-videos">
    <?php foreach ($pages->get("/videos")->videos as $video): ?>
        <div class="col-sm-6">
            <div class="video-wrapper">
                <?= $video->video_embed; ?>
            </div>
        </div>
    <?php endforeach; ?>
</div>

<?php

include("./foot.inc");


<?php

/**
 * Demo site header include file (HTML5)
 *
 * Note that this file has nothing to do with ProcessWire. We just split our common 
 * header and footer markup into separate files (head.inc and foot.inc) like this, 
 * since it was common to all of our templates. 
 *
 */

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<? $page->summary; ?>">

    <title><?= $page->headline; ?></title>

    <link href="<?= $config->urls->templates?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?= $config->urls->templates?>assets/css/base.css" rel="stylesheet">

    <link rel="shortcut icon" href="<?= $config->urls->templates?>assets/imgs/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?= $config->urls->templates?>assets/imgs/favicon.ico" type="image/x-icon">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
</head>

<body>

<nav class="nav navbar navbar-fixed-top navbar-inverse" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Base Template</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <?php
                $homepage = $pages->get("/");
                $children = $homepage->children;
                $children->prepend($homepage);

                foreach($children as $child) {
                    $class = $child === $page->rootParent ? " class='active'" : '';
                    echo "<li$class ><a href='{$child->url}'>{$child->title}</a></li>";
                }
                ?>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container -->
</nav>
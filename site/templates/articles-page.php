<?php

/**
 * Page template
 *
 */

include("./head.inc");

?>

    <div class="page-main">
        <div class="container">
            <?= $page->body; ?>
        </div>
    </div>
    <div class="main-container container">
        <h2>Articles</h2>
        <?php foreach ($pages->get("/articles")->articles as $article): ?>
            <h3><?=$article->article_title?></h3>
            <? if (!is_null($article->article_summary)): ?>
                <p><?=$article->article_summary?></p>
            <? endif; ?>
            <a href="<?=$article->article_url?>">View Article</a>
        <?php endforeach; ?>
    </div>

<?php

include("./foot.inc");


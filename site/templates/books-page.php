<?php

/**
 * Page template
 *
 */

include("./head.inc");

?>

    <div class="page-main">
        <div class="container">
            <?= $page->body; ?>
        </div>
    </div>
    <div class="main-container container">
        <h2>Books</h2>
        <?php foreach ($pages->get("/books")->books as $book): ?>
            <h3><?=$book->book_title?></h3>
            <? if (!is_null($book->book_summary)): ?>
                <p><?=$book->book_summary?></p>
            <? endif; ?>
            <a href="<?=$book->book_url?>">View Book</a>
        <?php endforeach; ?>
    </div>

<?php

include("./foot.inc");

